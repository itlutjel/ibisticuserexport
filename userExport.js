/***************************************************************************
	Reads the Ibistic parameters file and writes a list of company ids
	to a parameterfile
	------------------------------------------------------------------------
	Node does not support iso-8859-1 (ansi), but reads an xml in that
	encoding using utf8. Try using utf-16 (utf16le) first, as it does not
	fail, but only finds no nodes. If the node list is 0 then try utf8.
	That works both with ansi and utf8.

	Note that reading ansi æøå with utf8 will result in funny letters
*****************************************************************************/

	//File system
	var fs = require("fs");

	//Parametervalues from separate file
	//Production parameters
	var params = require("./userExportParms.js");

	//Define the database connection paramaters
	var mssql = require('mssql');
	var config = {
		user: params.user,
		password: params.password,
		server: params.server,
		database: params.database,
	}

	//Datetime utillity. Set no language for weekdays
	var moment = require("moment");

	//Define the name of the logfile
	//Logfiles are kept as rotating weekday_hour
	var companylistfile = "iteMemberCompanies.txt";

	//The company list is read from the parameter file that Ibistic maintains on our server
	var companies = new Array();

	//Use test parameters if the comand line is supplied with an argument = test
	if(process.argv.length > 2) {
		if(process.argv[2].toLowerCase() == 'test') params = require("./TestUserExportParms.js");
	}

	//Write the company list to the file
	fillCompanies();
	fs.writeFileSync(companylistfile, companies.join("\r\n"));



function fillCompanies() {

	//Read the xml file into a string to parse into an xml (!)
	//Try unicode16 encoding first. That won't fail to parse even if the encoding is different
	var DOMParser = require('xmldom').DOMParser;
	var keyCount = 0;

	try {
		//Read and load the parameter.xml
		var xmlString = fs.readFileSync(params.configPath, {encoding: 'utf16le'}).toString();
		var doc = new DOMParser().parseFromString(xmlString, 'text/xml');

		//Try to get the list of company ids
		var keyList = doc.getElementsByTagName(params.keyTagName);
		keyCount = keyList.length;

		//Log the count
		logtxt = 'UTF-16-nodes count: ' + keyCount + '\n';
		fs.appendFileSync(logfile, logtxt);

	} catch(e) { fs.appendFileSync(logfile, e.message); }

	//If no items were found, the xml is not a utf16.
	//Try to load it as utf8. This will handle both utf8 and ansi, keeping in mind that
	//reading ansi as utf8 wil destroy æøå
	if(keyCount == 0) {
		var xmlString = fs.readFileSync(params.configPath, {encoding: 'utf8'}).toString();
		var doc = new DOMParser().parseFromString(xmlString, 'text/xml');
		var keyList = doc.getElementsByTagName(params.keyTagName);
		keyCount = keyList.length;
		logtxt = 'UTF-8-nodes count:' + keyCount + '\n';
		fs.appendFileSync(logfile, logtxt);
	}

	//If the key list is still empty this should be logged
	if(keyCount == 0) {
		logtxt = "Ingen selskapsnoder verken som utf-16 eller utf-8\n";
		console.log(logtxt);
		fs.appendFileSync(logfile, logtxt);
		return;
	}

	//Read the company ids from active companies into the companies array
	for(var i = 0; i < keyList.length; i++) {

		//Keep the company id only if the parent node is labeled enabled="true"
		var active = keyList[i].parentNode.getAttribute("enabled");
		if(active == 'true') {
			company = keyList[i].textContent;
			companies.push(company.toLowerCase());
		}
	}

	//Log the companies count
	logtxt = "Active companies count: " + companies.length + '\n';
	console.log(logtxt);
	fs.appendFileSync(logfile, logtxt);

	//Extract the charts for each company into text files
	companies.sort();

}

function writeUserFile(company) {
	//Writes one single users-file
	//Calls a stored proc that returns all needed data
	var query = params.query.replace('@', company);

	//Create a connection using the global mssql object
	var connection = new mssql.Connection(config, function(err) {
		if(err) {
			console.log(err);
			connection.close();
			return false;
		}

		//Use a request to run the query and return a recordset
		var request = connection.request();
		request.query(query, function(err, recordset) {
			if(err) {
				console.log(err);
				connection.close();
				return false;
			}

			//Write each line to a string variable
			//Each record is one accounting chart code item
			filecontent = "Dataareaid;Department;Employee;Firstname;Familyname;Email;Street;Zipcode;City;Bankaccount;Status\r\n";
			for(j = 0; j < recordset.length; j++) {
				record = recordset[j];
				filecontent += record.Dataareaid
				filecontent += ';'+record.Department
				filecontent += ';'+record.Employee
				filecontent += ';'+record.Firstname
				filecontent += ';'+record.Familyname
				filecontent += ';'+record.Email
				filecontent += ';'+record.Street
				filecontent += ';'+record.Zipcode
				filecontent += ';'+record.City
				filecontent += ';'+record.Bankaccount
				filecontent += ';'+record.Status + "\n";
			}

			//Define the output file for this company
			filename = params.userFile.replace('@', company);

			//Write the query result to the output file and close the connection
			fs.writeFileSync(filename, filecontent);
			filesCompleted++;
			nbr = ('  '+filesCompleted).substr(-2);

			var logtxt = nbr + " " + filename + moment().format(' [at] hh:mm:ss[\n]');
			console.log(logtxt);
			fs.appendFileSync(logfile, logtxt);
			connection.close();

			//If this was the last company, log all completed
			if(filesCompleted == companies.length) {
				logtxt = moment().format('[All companies completed] hh:mm:ss[\n]');
				console.log(logtxt);
				fs.appendFileSync(logfile, logtxt);
			}
		});
	});
}

